package nl.utwente.di.toFahrenheit;

public class ToFahrenheit {

    double toFahrenheit(String text) {
        double celsius = Double.parseDouble(text);
        return (celsius * 1.8) + 32;
    }
}
